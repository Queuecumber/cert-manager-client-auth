import argparse
import yaml
import kubernetes
import kubernetes.client.rest
import base64
import gnupg
import io
from OpenSSL import crypto
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication


cert_template = """
apiVersion: certmanager.k8s.io/v1alpha1
kind: Certificate
metadata:
  name: {name}-cert
  namespace: {namespace}
spec:
  secretName: {name}-auth
  issuerRef:
    name: {issuerRef[name]}
    kind: {issuerRef[kind]}
  commonName: {email}
"""


def import_keys(clients):
    gpg = gnupg.GPG()

    import_keys = []
    for c in clients:
        if 'keyid' not in c:
            search = gpg.search_keys(c['email'], 'hkp://pool.sks-keyservers.net')
            if len(search) > 0:
                c['keyid'] = search[0]['keyid']
            else:
                print('WARNING: no key for {}'.format(c['email']))

        import_keys.append(c['keyid'])

    gpg.recv_keys('hkp://pool.sks-keyservers.net', *import_keys)

    return gpg


def request_certificate(c):
    coa_client = kubernetes.client.CustomObjectsApi()
    cert_object = yaml.load(cert_template.format_map(c))

    try:
        coa_client.create_namespaced_custom_object(
            group='certmanager.k8s.io',
            version='v1alpha1',
            namespace=c['namespace'],
            plural='certificates',
            body=cert_object
        )
    except kubernetes.client.rest.ApiException as e:
        if e.status == 409:
            coa_client.patch_namespaced_custom_object(
                name='{}-cert'.format(c['name']),
                group='certmanager.k8s.io',
                version='v1alpha1',
                namespace=c['namespace'],
                plural='certificates',
                body=cert_object
            )


def email_cert(recipient, cert, pw):
    message = MIMEMultipart()

    message['From'] = 'issuer@certs.queuecumber.net'
    message['To'] = recipient
    message['Subject'] = 'New Certificate Issued'

    form_message = """
    A new client certificate has been issued to you. 
    The new certificate is attached and has been encrypted with your public key.
    Please do not reply to this message.
    """

    message.attach(MIMEText(form_message, 'plain'))
    attached_cert = MIMEApplication(cert, _subtype='asc')
    attached_cert.add_header('content-disposition', 'attachment', filename='{}.pfx.asc'.format(recipient))
    message.attach(attached_cert)

    smtp = smtplib.SMTP('smtp.sparkpostmail.com', port=587)
    smtp.starttls()
    smtp.login('SMTP_Injection', pw)
    smtp.sendmail('issuer@certs.queuecumber.net', recipient, message.as_string())
    smtp.close()
    print('SUCCESS: sent updated cert to {}'.format(recipient))


def notify_client(client, secret, gpg, pw):
    cert_chain = base64.b64decode(secret.data['tls.crt'])
    priv_key = base64.b64decode(secret.data['tls.key'])

    cert_data = crypto.load_certificate(crypto.FILETYPE_PEM, cert_chain)
    key_data = crypto.load_privatekey(crypto.FILETYPE_PEM, priv_key)

    p12 = crypto.PKCS12()
    p12.set_certificate(cert_data)
    p12.set_privatekey(key_data)

    p12file = p12.export()

    with io.BytesIO(p12file) as b:
        encrypted_p12 = gpg.encrypt_file(b, client['keyid'], armor=False)

    email_cert(client['email'], str(encrypted_p12), pw)


def wait_for_secrets(clients, gpg, pw):
    v1_client = kubernetes.client.CoreV1Api()
    watcher = kubernetes.watch.Watch()

    events = watcher.stream(v1_client.list_secret_for_all_namespaces, pretty=True)

    for ev in events:
        ev_locator = {'name': ev['object'].metadata.name, 'namespace': ev['object'].metadata.namespace}
        matches = list(filter(lambda x: '{}-auth'.format(x['name']) == ev_locator['name'], clients))

        if len(matches) > 0:
            print(ev['type'], matches)
            if ev['type'] == 'ADDED' or ev['type'] == 'MODIFIED':
                notify_client(matches[0], ev['object'], gpg, pw)


def main(args):
    with open(args.config) as f:
        config = yaml.load(f)

    if args.kubeconfig is None:
        kubernetes.config.load_incluster_config()
    else:
        kubernetes.config.load_kube_config(args.kubeconfig)

    gpg = import_keys(config['clients'])

    for c in config['clients']:
        request_certificate(c)

    wait_for_secrets(config['clients'], gpg, args.smtppw)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('config', help='Path to the config file')
    parser.add_argument('smtppw', help='Password for SMTP server')
    parser.add_argument('kubeconfig', help='Path to kubeconfig for out-of-cluster', default=None)
    args = parser.parse_args()
    main(args)
